/* eslint-disable max-len */
import { Router } from 'express';
import { Volunteer, User, Event } from '../database/models';

const router = Router();

const isValidVolunteer = async (res, id) => {
  const volunteer = await Volunteer.findByPk(id);
  if (!volunteer) {
    res.status(400).jsend.error('You must provide a valid volunteer id !');
    return false;
  }
  return true;
};

router.post('/create', async (req, res, next) => {
  try {
    const {
      email,
      password,
      firstname,
      lastname,
      birthday,
      gender,
    } = req.body;

    if (!email || !password || !firstname || !lastname || !birthday || !gender) {
      res.status(400).jsend.error('You must provide an email, password, firstname, lastname, birthday and gender');
      return;
    }

    const user = await User.create({ email, password });
    const newVolunteer = await Volunteer.create(req.body);
    newVolunteer.setUser(user);

    res.status(201).jsend.success(newVolunteer);
  } catch (error) {
    next(error);
  }
});

router.post('/login', async (req, res, next) => {
  try {
    const { email, password } = req.body;

    if (!email || !password) {
      res.status(400).jsend.error('You must provide an email and password to login');
      return;
    }

    const user = await User.findOne({ where: { email } });
    if (!user || !(await user.checkPassword(password))) {
      res.status(400).jsend.error('Your email or password is wrong !');
      return;
    }

    const volunteer = await Volunteer.findOne({ where: { userId: user.id }, include: [User] });

    res.jsend.success(volunteer);
  } catch (error) {
    next(error);
  }
});

router.put('/update/:volunteerId(\\d+)', async (req, res, next) => {
  try {
    const { volunteerId: id } = req.params;
    const isValid = await isValidVolunteer(res, id);
    if (!isValid) return;

    const updatedVolunteer = await Volunteer.update(
      req.body,
      { returning: true, plain: true, where: { id } },
    );

    await User.update(
      req.body,
      { returning: true, where: { id: updatedVolunteer[1].userId } },
    );

    const volunteer = await Volunteer.findByPk(id, {
      include: [{ model: User, attributes: ['email', 'id'] }],
    });

    res.jsend.success(volunteer);
  } catch (error) {
    next(error);
  }
});

router.delete('/delete/:volunteerId(\\d+)', async (req, res, next) => {
  try {
    const { volunteerId: id } = req.params;
    const isValid = await isValidVolunteer(res, id);
    if (!isValid) return;

    await Volunteer.destroy({ where: { id } });
    await User.destroy({ where: { id } });

    res.jsend.success('Volunteer successfully deleted !');
  } catch (error) {
    next(error);
  }
});

router.get('/', async (req, res, next) => {
  try {
    const allVolunteers = await Volunteer.findAll({
      include: [{ model: User, attributes: ['email', 'id'] }],
    });
    res.jsend.success(allVolunteers);
  } catch (error) {
    next(error);
  }
});

router.get('/:volunteerId(\\d+)', async (req, res, next) => {
  try {
    const { volunteerId: id } = req.params;
    const isValid = await isValidVolunteer(res, id);
    if (!isValid) return;

    const association = await Volunteer.findByPk(id, {
      include: [{ model: User, attributes: ['email', 'id'] }],
    });
    res.jsend.success(association);
  } catch (error) {
    next(error);
  }
});

router.get('/:volunteerId/events', async (req, res, next) => {
  try {
    const { volunteerId: id } = req.params;
    const volunteer = await Volunteer.findByPk(id);

    const volunteerEvents = await volunteer.getEvents();

    res.jsend.success(volunteerEvents);
  } catch (error) {
    next(error);
  }
});

router.get('/:volunteerId/events/:eventId', async (req, res, next) => {
  try {
    const { volunteerId, eventId } = req.params;
    const volunteer = await Volunteer.findByPk(volunteerId);
    const event = await Event.findByPk(eventId);

    if (!volunteer) res.status(400).jsend.error('You must provide a valid volunteer id !');
    if (!event) res.status(400).jsend.error('You must provide a valid event id !');

    const volunteerEvent = await volunteer.hasEvents(event);

    res.jsend.success({ isParticipating: volunteerEvent });
  } catch (error) {
    next(error);
  }
});

router.post('/:volunteerId/events/:eventId', async (req, res, next) => {
  try {
    const { volunteerId, eventId } = req.params;
    const volunteer = await Volunteer.findByPk(volunteerId);
    const event = await Event.findByPk(eventId);

    if (!volunteer) res.status(400).jsend.error('You must provide a valid volunteer id !');
    if (!event) res.status(400).jsend.error('You must provide a valid event id !');

    volunteer.addEvent(event);
    res.jsend.success(event);
  } catch (error) {
    next(error);
  }
});

router.delete('/:volunteerId/events/:eventId', async (req, res, next) => {
  try {
    const { volunteerId, eventId } = req.params;
    const volunteer = await Volunteer.findByPk(volunteerId);
    const event = await Event.findByPk(eventId);

    if (!volunteer) res.status(400).jsend.error('You must provide a valid volunteer id !');
    if (!event) res.status(400).jsend.error('You must provide a valid event id !');

    const deletedVolunteerEvent = await volunteer.removeEvents(event);
    if (deletedVolunteerEvent <= 0) {
      res.jsend.error('Failed removing participation of volunteer');
    }

    res.jsend.success({ isParticipating: false });
  } catch (error) {
    next(error);
  }
});

export default router;
