/* eslint-disable max-len */
import { Router } from 'express';
import { Association, User } from '../database/models';

const router = Router();

const isValidAssos = async (res, id) => {
  const assos = await Association.findByPk(id);
  if (!assos) {
    res.status(400).jsend.error('You must provide a valid association id !');
    return false;
  }
  return true;
};

router.post('/create', async (req, res, next) => {
  try {
    const {
      email,
      password,
      name,
      manager,
      description,
      phone,
      address,
    } = req.body;

    if (!email || !password || !name || !manager) {
      res.status(400).jsend.error('You must provide an email, password, name and a manager');
      return;
    }

    const user = await User.create({ email, password });
    const newAssociation = await Association.create({
      name,
      manager,
      description,
      phone,
      address,
    });
    await newAssociation.setUser(user);

    const association = await Association.findByPk(newAssociation.id, {
      include: [User],
    });
    res.status(201).jsend.success(association);
  } catch (error) {
    next(error);
  }
});

router.post('/login', async (req, res, next) => {
  try {
    const { email, password } = req.body;

    if (!email || !password) {
      res.status(400).jsend.error('You must provide an email and password to login');
      return;
    }

    const user = await User.findOne({ where: { email } });
    if (!user || !(await user.checkPassword(password))) {
      res.status(400).jsend.error('Your email or password is wrong !');
      return;
    }

    const association = await Association.findOne({ where: { userId: user.id }, include: [User] });

    res.jsend.success(association);
  } catch (error) {
    next(error);
  }
});

router.put('/update/:associationId(\\d+)', async (req, res, next) => {
  try {
    const { associationId: id } = req.params;
    const isValid = await isValidAssos(res, id);
    if (!isValid) return;

    const updatedAssociation = await Association.update(
      req.body,
      { returning: true, where: { id } },
    );

    const { userId } = updatedAssociation[1][0];

    await User.update(
      req.body,
      { where: { id: userId } },
    );

    const association = await Association.findByPk(id, {
      include: [{ model: User }],
    });

    res.jsend.success(association);
  } catch (error) {
    next(error);
  }
});

router.delete('/delete/:associationId(\\d+)', async (req, res, next) => {
  try {
    const { associationId: id } = req.params;
    const isValid = await isValidAssos(res, id);
    if (!isValid) return;

    await Association.destroy({ where: { id } });
    await User.destroy({ where: { id } });

    res.jsend.success('Association successfully deleted !');
  } catch (error) {
    next(error);
  }
});

router.get('/', async (req, res, next) => {
  try {
    const allAssociations = await Association.findAll({
      include: [{ model: User }],
    });
    res.jsend.success(allAssociations);
  } catch (error) {
    next(error);
  }
});

router.get('/:associationId(\\d+)', async (req, res, next) => {
  try {
    const { associationId: id } = req.params;
    const isValid = await isValidAssos(res, id);
    if (!isValid) return;

    const association = await Association.findByPk(id, {
      include: [{ model: User }],
    });
    res.jsend.success(association);
  } catch (error) {
    next(error);
  }
});

router.get('/:associationId(\\d+)/events', async (req, res, next) => {
  try {
    const { associationId: id } = req.params;
    const isValid = await isValidAssos(res, id);
    if (!isValid) return;

    const association = await Association.findByPk(id);
    const associationsEvents = await association.getEvents();

    res.jsend.success(associationsEvents);
  } catch (error) {
    next(error);
  }
});

export default router;
