import { Router } from 'express';
import { Availability } from '../database/models';

const router = Router();

const isValidAvailability = async (res, id) => {
  const availability = await Availability.findByPk(id);
  if (!availability) {
    res.status(204).jsend.error('You must provide a valid availability id !');
    return false;
  }
  return true;
};

// Create Availability
router.post('/create', async (req, res, next) => {
  try {
    const {
      name,
      description,
    } = req.body;
    const newAvailability = await Availability.create({
      name,
      description,
    });

    res.status(201).jsend.success(newAvailability);
  } catch (error) {
    next(error);
  }
});

// Read all Availabilities
router.get('/', async (req, res, next) => {
  try {
    const allAvailabilities = await Availability.findAll();

    if (!allAvailabilities) {
      res.status(204).jsend.error('no data');
    }

    res.status(200).jsend.success(allAvailabilities);
  } catch (error) {
    next(error);
  }
});

// Read one Availability
router.get('/:availabilityId(\\d+)', async (req, res, next) => {
  try {
    const { availabilityId: id } = req.params;
    const availability = await Availability.findByPk(id);

    if (!availability) {
      res.status(204).jsend.error('no data');
    }

    res.status(200).jsend.success(availability);
  } catch (error) {
    next(error);
  }
});

// Update Availability
router.put('/update/:availabilityId(\\d+)', async (req, res, next) => {
  try {
    const { availabilityId: id } = req.params;
    const isValid = await isValidAvailability(res, id);
    if (!isValid) return;

    const updatedAvailability = await Availability.update(
      req.body,
      { returning: true, where: { id } },
    );

    res.status(200).jsend.success(updatedAvailability);
  } catch (error) {
    next(error);
  }
});

// Delete Availability
router.delete('/delete/:availabilityId(\\d+)', async (req, res, next) => {
  try {
    const { availabilityId: id } = req.params;
    const isValid = await isValidAvailability(res, id);
    if (!isValid) return;

    const deletedAvailability = await Availability.destroy({ where: { id } });

    res.status(200).jsend.success(deletedAvailability);
  } catch (error) {
    next(error);
  }
});


export default router;
