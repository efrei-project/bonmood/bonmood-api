import { Router } from 'express';
import { Tag } from '../database/models';

const router = Router();

const isValidTag = async (res, id) => {
  const tag = await Tag.findByPk(id);
  if (!tag) {
    res.status(400).jsend.error('You must provide a valid tag id !');
    return false;
  }
  return true;
};

// Create Tag
router.post('/create', async (req, res, next) => {
  try {
    const { name, description } = req.body;
    if (!name || !description) {
      res.status(400).jsend.error('You must provide a name and description to add a tag !');
    }

    const newTag = await Tag.create(req.body);

    res.status(201).jsend.success(newTag);
  } catch (error) {
    next(error);
  }
});


// Read all Tags
router.get('/', async (req, res, next) => {
  try {
    const allTags = await Tag.findAll();
    res.jsend.success(allTags);
  } catch (error) {
    next(error);
  }
});

// Read one Tag
router.get('/:tagId(\\d+)', async (req, res, next) => {
  try {
    const { tagId: id } = req.params;
    const isValid = await isValidTag(res, id);
    if (!isValid) return;

    const tag = await Tag.findByPk(id);

    res.jsend.success(tag);
  } catch (error) {
    next(error);
  }
});

// Update Tag
router.put('/update/:tagId(\\d+)', async (req, res, next) => {
  try {
    const { tagId: id } = req.params;
    const isValid = await isValidTag(res, id);
    if (!isValid) return;

    await Tag.update(req.body, { returning: true, where: { id } });

    const updatedTag = await Tag.findByPk(id);

    res.jsend.success(updatedTag);
  } catch (error) {
    next(error);
  }
});

// Delete Tag
router.delete('/delete/:tagId(\\d+)', async (req, res, next) => {
  try {
    const { tagId: id } = req.params;
    const isValid = await isValidTag(res, id);
    if (!isValid) return;

    await Tag.destroy({ where: { id } });

    res.jsend.success({});
  } catch (error) {
    next(error);
  }
});

export default router;
