import { Router } from 'express';
import Events from './events';
import Associations from './associations';
import Volunteers from './volunteers';
import Tags from './tags';
import Availabilities from './availabilities';

const router = Router();

router.get('/', (req, res) => res.json({ message: 'API Bonmood' }));

router.use('/events', Events);
router.use('/associations', Associations);
router.use('/volunteers', Volunteers);
router.use('/tags', Tags);
router.use('/availabilities', Availabilities);

export default router;
