/* eslint-disable max-len */
import { Router } from 'express';
import { Op } from 'sequelize';
import { Event, Tag } from '../database/models';

const router = Router();

const isValidEvent = async (res, id) => {
  const event = await Event.findByPk(id);
  if (!event) {
    res.status(204).jsend.error('You must provide a valid event id !');
    return false;
  }
  return true;
};

// Create Event
router.post('/create', async (req, res, next) => {
  try {
    const {
      name,
      place,
      number,
      dateBegin,
      dateEnd,
      description,
      associationId,
      tags = [],
      latitude,
      longitude,
    } = req.body;

    const tmp = [
      'https://aleteiafrench.files.wordpress.com/2019/02/france-macron-maraude-paris-samu.jpg',
      'https://static.actu.fr/uploads/2017/11/restos-du-coeur1-854x569.jpg',
    ];

    if (!name || !place || !number || !latitude || !longitude || !dateBegin || !dateEnd || !description || !associationId) {
      res.status(400).jsend.error('You must provide name, place, number, latitude, longitude, dateBegin, dateEnd, description and the associationId');
      return;
    }

    const allTags = await Tag.findAll({
      where: {
        id: {
          [Op.in]: tags,
        },
      },
    });
    if (allTags.length !== tags.length) {
      res.status(400).jsend.error('Some of tags provided, are not exists');
      return;
    }

    const newEvent = await Event.create(
      { ...req.body, addressImg: tmp[1] },
      { returning: true },
    );
    await newEvent.addTags(tags);

    const events = await Event.findByPk(newEvent.id, {
      include: [Tag],
    });

    res.status(201).jsend.success(events);
  } catch (error) {
    next(error);
  }
});

// Read all Events
router.get('/', async (req, res, next) => {
  try {
    const allEvents = await Event.findAll({
      include: [Tag],
    });
    res.jsend.success(allEvents);
  } catch (error) {
    next(error);
  }
});

// Read one Event
router.get('/:eventId(\\d+)', async (req, res, next) => {
  try {
    const { eventId: id } = req.params;
    const isValid = await isValidEvent(res, id);
    if (!isValid) return;

    const event = await Event.findByPk(id, {
      include: [Tag],
    });
    res.jsend.success(event);
  } catch (error) {
    next(error);
  }
});

// Update Event
router.put('/update/:eventId(\\d+)', async (req, res, next) => {
  try {
    const { eventId: id } = req.params;
    const isValid = await isValidEvent(res, id);
    if (!isValid) return;

    await Event.update(req.body, { returning: true, where: { id } });

    const { tags = '' } = req.body;
    const eventToUpdate = await Event.findByPk(id);
    await eventToUpdate.setTags(tags.split(','));

    const event = await Event.findByPk(id, { include: [Tag] });

    res.jsend.success(event);
  } catch (error) {
    next(error);
  }
});

// Delete Event
router.delete('/delete/:eventId(\\d+)', async (req, res, next) => {
  try {
    const { eventId: id } = req.params;
    const isValid = await isValidEvent(res, id);
    if (!isValid) return;

    await Event.destroy({ where: { id } });

    res.jsend.success({});
  } catch (error) {
    next(error);
  }
});

router.get('/:eventId(\\d+)/participant', async (req, res, next) => {
  try {
    const { eventId: id } = req.params;
    const isValid = await isValidEvent(res, id);
    if (!isValid) return;

    const event = await Event.findByPk(id);
    const volunteersFromEvent = await event.getVolunteers();

    res.jsend.success(volunteersFromEvent);
  } catch (error) {
    next(error);
  }
});

export default router;
