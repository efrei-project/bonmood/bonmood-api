import express from 'express';
import jsend from 'jsend';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import { config as dotEnvConfig } from 'dotenv';
import errorHandler from './middlewares/errorHandler';
import routes from './routes';
import db from './database/initDatabase';

dotEnvConfig();

const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'development';

const app = express();

app.use(morgan('dev'));

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use(jsend.middleware);

app.use('/', routes);

app.use(errorHandler);

db.authenticate()
  .then(async () => {
    console.log('Connection has been established successfully.');
    if (env === 'production') {
      await db.sync({ alter: true });
    } else {
      await db.sync({ force: true });
    }

    app.listen(port, (error) => {
      if (error) {
        throw new Error(error);
      }

      console.log('Server listening at port %d', port);
    });
  })
  .catch(err => console.log('Connection to the database has failed.', err));

export default app;
