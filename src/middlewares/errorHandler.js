import messages from '../configs/languages/errors_fr';

const errorMiddleware = (error, req, res, next) => {
  if (messages[error.name]) {
    const { status, message } = messages[error.name];
    res.status(status).jsend.error(message);
  } else if (error.errors) {
    res.status(500).jsend.error(error.errors.map(e => e.message).join(', '));
  } else {
    res.status(500).jsend.error(error);
  }
  next();
};

export default errorMiddleware;
