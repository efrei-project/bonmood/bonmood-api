const messages = {
  UnknownError: {
    status: 500,
    message: 'Une erreur inconnue est survenue',
  },
  SequelizeConnectionError: {
    status: 500,
    message: 'Une erreur est survenue lors de la connexion à la base de données.',
  },
  SequelizeConnectionRefusedError: {
    status: 500,
    message: 'Connexion refusée à la base de données',
  },
  SequelizeUniqueConstraintError: {
    status: 409,
    message: 'Un enregistrement existe déjà.',
  },
  SequelizeDatabaseError: {
    status: 500,
    message: 'Une erreur est survenue lors de la requête à la base de données.',
  },
  CredentialsError: {
    status: 403,
    message: 'Les identifiants fournis ne sont pas valides.',
  },
};

export default messages;
