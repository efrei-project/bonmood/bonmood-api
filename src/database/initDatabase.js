import Sequelize from 'sequelize';
import { config as dotEnvConfig } from 'dotenv';
import {
  Association,
  Availability,
  Event,
  Tag,
  User,
  Volunteer,
} from './models/index';

dotEnvConfig();

const env = process.env.NODE_ENV || 'development';
const config = require('./sequelize.config.js')[env];

const db = env === 'development'
  ? new Sequelize(
    config.database,
    config.username,
    config.password,
    config,
  )
  : new Sequelize(config.databaseUrl, config);

User.init(db);
Association.init(db);
Volunteer.init(db);
Availability.init(db);
Tag.init(db);
Event.init(db);

Association.associate();
Volunteer.associate();
Availability.associate();
Event.associate();

export default db;
