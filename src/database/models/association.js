import { Model, DataTypes } from 'sequelize';
import User from './user';
import Event from './event';


class Association extends Model {
  static init(database) {
    return super.init(
      {
        name: DataTypes.STRING,
        manager: DataTypes.STRING,
        description: DataTypes.STRING,
        phone: DataTypes.STRING,
        address: DataTypes.STRING,
      }, {
        tableName: 'Association',
        sequelize: database,
      },
    );
  }
}

Association.associate = () => {
  Association.belongsTo(User, { foreignKey: 'userId' });
  Association.hasMany(Event, { foreignKey: 'associationId' });
};

export default Association;
