import { Model, DataTypes } from 'sequelize';

class Tag extends Model {
  static init(database) {
    return super.init(
      {
        name: DataTypes.STRING,
        description: DataTypes.STRING,
      }, {
        tableName: 'Tag',
        sequelize: database,
      },
    );
  }
}

Tag.associate = () => { };

export default Tag;
