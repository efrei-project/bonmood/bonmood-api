import { Model, DataTypes } from 'sequelize';

class Availability extends Model {
  static init(database) {
    return super.init(
      {
        dateBegin: DataTypes.DATE,
        dateEnd: DataTypes.DATE,
      }, {
        tableName: 'Availability',
        sequelize: database,
      },
    );
  }
}

Availability.associate = () => { };

export default Availability;
