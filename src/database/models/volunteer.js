import { Model, DataTypes } from 'sequelize';
import User from './user';
import Availability from './availability';
import Event from './event';

class Volunteer extends Model {
  static init(database) {
    return super.init(
      {
        firstname: DataTypes.STRING,
        lastname: DataTypes.STRING,
        birthday: DataTypes.DATEONLY,
        gender: DataTypes.STRING,
      }, {
        tableName: 'Volunteer',
        sequelize: database,
      },
    );
  }
}

Volunteer.associate = () => {
  Volunteer.belongsTo(User, { foreignKey: 'userId' });
  Volunteer.hasMany(Availability, { foreignKey: 'volunteerId' });
  Volunteer.belongsToMany(Event, { through: 'VolunteerEvent' });
};

export default Volunteer;
