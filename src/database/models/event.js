import { Model, DataTypes } from 'sequelize';
import Tag from './tag';
import Volunteer from './volunteer';

class Event extends Model {
  static init(database) {
    return super.init(
      {
        name: DataTypes.STRING,
        place: DataTypes.STRING,
        number: DataTypes.INTEGER,
        dateBegin: DataTypes.DATE,
        dateEnd: DataTypes.DATE,
        description: DataTypes.STRING,
        latitude: DataTypes.DOUBLE,
        longitude: DataTypes.DOUBLE,
        addressImg: DataTypes.STRING,
      }, {
        tableName: 'Event',
        sequelize: database,
      },
    );
  }
}

Event.associate = () => {
  Event.belongsToMany(Volunteer, { through: 'VolunteerEvent' });
  Event.belongsToMany(Tag, { through: 'EventTag' });
};

export default Event;
