/* eslint-disable no-param-reassign */
import { Model, DataTypes } from 'sequelize';
import bcrypt from 'bcryptjs';

class User extends Model {
  static init(database) {
    return super.init(
      {
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            isEmail: { msg: 'Email is not valid.' },
            notEmpty: true,
          },
          unique: {
            args: true,
            msg: 'Email already used by another user',
          },
        },
        passwordDigest: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
        password: {
          type: DataTypes.VIRTUAL,
          validate: {
            isLongEnough(v) {
              if (v.length < 7) {
                throw new Error('Password must have at least 7 characters');
              }
            },
          },
        },
      }, {
        tableName: 'User',
        sequelize: database,
        indexes: [
          {
            unique: true,
            fields: ['email'],
          },
        ],
        // defaultScope: {
        //   attributes: { exclude: ['passwordDigest', 'password'] },
        // },
        hooks: {
          async beforeValidate(userInstance) {
            if (userInstance.isNewRecord) {
              userInstance.passwordDigest = await userInstance.generateHash();
            }
          },

          async beforeSave(userInstance) {
            if (!userInstance.isNewRecord && userInstance.changed('password')) {
              userInstance.passwordDigest = await userInstance.generateHash();
            }
          },

          async beforeUpdate(userInstance) {
            if (userInstance.password && userInstance.changed('password')) {
              userInstance.passwordDigest = await userInstance.generateHash();
            }
          },
        },
      },
    );
  }

  async generateHash() {
    if (!this.password) {
      return;
    }

    const SALT_ROUND = 5;
    const hashed = bcrypt.hash(this.password, SALT_ROUND);

    if (!hashed) {
      throw new Error("Password can't be hashed!");
    }

    // eslint-disable-next-line consistent-return
    return hashed;
  }

  checkPassword(password) {
    return bcrypt.compare(password, this.passwordDigest);
  }

  toJSON() {
    const values = Object.assign({}, this.get());

    delete values.passwordDigest;
    delete values.password;

    return values;
  }
}

User.associate = () => { };

export default User;
