import app from '../src/app';

const request = require('supertest');

describe('APP Routes', () => {
  describe('/', () => {
    describe('Method GET', () => {
      test('It should return statusCode 200', async () => {
        const response = await request(app).get('/').set({ 'Accept-Language': 'fr-FR' });

        expect(response.statusCode).toBe(200);
      });

      test('It should return a json: {"message": "API Bonmood"}', async () => {
        const response = await request(app).get('/').set({ 'Accept-Language': 'fr-FR' });

        expect(response.body).toEqual({ message: 'API Bonmood' });
      });
    });
  });
});
